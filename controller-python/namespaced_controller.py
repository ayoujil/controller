from kubernetes import client, config, watch

# Charge la configuration OpenShift depuis les variables d'environnement KUBECONFIG
config.load_kube_config()

# Crée un objet pour interagir avec les ConfigMaps et les Namespaces
v1 = client.CoreV1Api()

# Définit le nom de la ConfigMap à surveiller
configmap_name = "tu-pra-config"

# Définit les options pour la surveillance des ConfigMaps
field_selector = "metadata.name=" + configmap_name
resource_version = ""

# Boucle de surveillance pour les événements ConfigMap
stream = watch.Watch().stream(v1.list_namespace,
                              field_selector=field_selector,
                              resource_version=resource_version)

for event in stream:
    # Extrait le type d'événement et l'objet Namespace mis à jour
    event_type = event['type']
    namespace = event['object']

    # Vérifie si l'événement est de type "DELETED"
    if event_type == "DELETED":
        print(f"ConfigMap {configmap_name} has been deleted in Namespace {namespace.metadata.name}!")

        # Vérifie si le Namespace a l'annotation correspondante
        if "annotations" in namespace.metadata:
            annotations = namespace.metadata.annotations
            if configmap_name in annotations:
                del annotations[configmap_name]
                namespace.metadata.annotations = annotations
                v1.patch_namespace(namespace.metadata.name, namespace.metadata)
                print(f"Removed annotation {configmap_name} from Namespace {namespace.metadata.name}")
            else:
                print(f"Annotation {configmap_name} not found in Namespace {namespace.metadata.name}")
        else:
            print(f"No annotations found in Namespace {namespace.metadata.name}")
    else:
        # Récupère le ConfigMap dans le Namespace courant

        for namespace in namespace_list:
            try:
                configmap = v1.read_namespaced_config_map(configmap_name, namespace.metadata.name)
            except client.rest.ApiException as e:
                if e.status == 404:
                    print(f"ConfigMap {configmap_name} not found in Namespace {namespace.metadata.name}!")
                    continue
                else:
                    raise e
        
            # Récupère les données du ConfigMap et ajoute chaque paire clé-valeur comme annotation au Namespace
            data = configmap.data
            annotations = namespace.metadata.annotations or {}
            annotations[configmap_name] = data
            namespace.metadata.annotations = annotations
            v1.patch_namespace(namespace.metadata.name, namespace.metadata)
        
            print(f"Added annotations to Namespace {namespace.metadata.name}:")
            print(annotations)