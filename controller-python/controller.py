from kubernetes import client, config, watch
import os

config.load_kube_config()
#config.load_incluster_config()

v1 = client.CoreV1Api()

configmap_name = "tu-pra-config"
tu_pra_namespace = os.getenv("NAMESPACE", "edf-tu-pra-poc") ## Supprimer la valeur par defaut
field_selector = "metadata.name=" + configmap_name
resource_version = ""

for event in watch.Watch().stream(v1.list_namespaced_config_map,
                              namespace=tu_pra_namespace,
                              field_selector=field_selector,
                              resource_version=resource_version):
    event_type = event['type']
    configmap_object = event['object']

    if event_type == "DELETED":
        print(f"Le configMap {configmap_name} a été supprimé")
        annotations = tu_pra_namespace.metadata.annotations
        if "openshift.io/node-selector" in annotations:
            del annotations["openshift.io/node-selector"]
            tu_pra_namespace.metadata.annotations = annotations
            v1.patch_namespace(tu_pra_namespace.metadata.name, tu_pra_namespace.metadata)
            print(f"L'annotation pour le TU PRA a été supprimée du namespace {tu_pra_namespace.metadata.name}")
        else:
            print(f"L'annotation pour le TU PRA n'existe pas dans le namesapce {tu_pra_namespace.metadata.name}")
                
    else:
        value = configmap_object.data["edf_site_annotation_value"]
        
        namespace = v1.read_namespace(tu_pra_namespace)
        namespace.metadata.annotations["openshift.io/node-selector"] = value
        v1.patch_namespace(tu_pra_namespace, namespace)
        print(f"L'annotation pour le TU PRA a été ajoutée dans le namespace {tu_pra_namespace}")

        pods_list = v1.list_namespaced_pod(tu_pra_namespace)
        print(f"Restart des pods suivants sur le namespace {tu_pra_namespace}")
        for pod in pods_list.items:
            print(f" * {pod.metadata.name}")
            v1.delete_namespaced_pod(pod.metadata.name, tu_pra_namespace)
            